﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {
	public PlayerController pc;
	public GameObject[] hazards;
	public GameObject debuff;
	public Vector3 spawnValues;
	public int hazardCount;
	public float spawnWait;
	public float startWait;
	public float waveWait;

	public GUIText scoreText;
	public GUIText restartText;
	public GUIText gameOverText;

	private bool gameOver;
	private bool restart;
	private int score;
	public Slider scoreSlider;


	void Start () 
	{
		score = 0;
		gameOver = false;
		restart = false;
		restartText.text = "";
		gameOverText.text = "";
		UpdateScore ();
		StartCoroutine (SpawnWaves ());
		scoreSlider.value = .0f;
	}

	void Update ()
	{
		if (restart) {
			if (Input.GetKeyDown (KeyCode.R)) {
				SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
			}
		}

		if (score >= 1500) {
			int newNumber = SceneManager.GetActiveScene ().buildIndex;
			newNumber += 1;
			if (newNumber <= 1)
			SceneManager.LoadScene (newNumber);
		}
	}



	IEnumerator SpawnWaves ()
	{
		yield return new WaitForSeconds (startWait);
		while (true)
		{
			for (int i = 0; i < hazardCount; i++) {
				GameObject hazard = hazards[Random.Range(0, hazards.Length)];
				Vector3 spawnPosition = new Vector3 (Random.Range (-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
				Quaternion spawnRotation = Quaternion.identity;
				if (Random.value <= .15f)
					Instantiate (debuff, spawnPosition, spawnRotation);
				else
				Instantiate (hazard, spawnPosition, spawnRotation);
				yield return new WaitForSeconds (spawnWait);
			}
			yield return new WaitForSeconds (waveWait);

			if (gameOver) {
				restartText.text = "Press 'R' to Restart";
				restart = true;
				break;
			}
		}
	}

	public void AddScore (int newScoreValue)
	{
		score += newScoreValue;
		UpdateScore ();
	}

	void UpdateScore ()
	{
			scoreText.text = "Score: " + score;
		if (scoreSlider.value <= 1f && pc.superRate == false) {
			scoreSlider.value += .1f;
			if (scoreSlider.value >= 1f) {
				pc.fireRate = 0;
				pc.superRate = true;
			}
		}
	}

	public void GameOver ()
	{
		gameOverText.text = "Game Over";
		gameOver = true;

	}
}
